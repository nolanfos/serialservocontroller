/*
Arduino driver for Android app remote control.
This sketch listens to instructions on the Serial port
then activates motors as required
Then sends back confirmation to the app
*/


#define MotA 11  //PWM output to set the motor speed
#define MotB 3   //PWM output to set the motor speed




void setup() {

  pinMode(MotA, OUTPUT);
  pinMode(MotB, OUTPUT);
  analogWrite(MotA, 200);  //set motor PWM to 200
  analogWrite(MotB, 200);
  
  Serial.begin(9600); 
}

void loop() {
  
  // see if there's incoming serial data:
  if (Serial.available() > 0) {
    // read the oldest byte in the serial buffer:
    int incomingByte = Serial.read();
    
    // action depending on the instruction
    // as well as sending a confirmation back to the app
    switch (incomingByte) {
      case 'F':
        moveForward(255, true);
        Serial.println("Going forward");
        break;
      case 'R':
        turn(255, true);
        Serial.println("Turning right");
        break;
      case 'L':
        turn(255, false);
        Serial.println("Turning left");
        break;
      case 'B':
        moveForward(255, false);
        Serial.println("Going backwards");
        break;
      case 'S':
        moveForward(0, true);
        Serial.println("Stopping");
        break;
      default: 
        // if nothing matches, do nothing
        break;
    }
  }
}

void moveForward(int speedBot, boolean forward){
//boolean forward controls motor direction
  if (forward){
      digitalWrite(MotA, HIGH);
      digitalWrite(MotA, HIGH);
    }
    else{
      digitalWrite(MotIN1, LOW);
      digitalWrite(MotIN2, LOW);
    }
    analogWrite(MotA, speedBot);
    analogWrite(MotB, speedBot);

}

void turn(int speedBot, boolean right){
  //boolean right controls motor direction
    if (right){
      digitalWrite(MotA, LOW);
      digitalWrite(MotB, HIGH);
    }
    else{
      digitalWrite(MotA, HIGH);
      digitalWrite(MotB, LOW);
    }
    analogWrite(MotA, speedBot);
    analogWrite(MotB, speedBot);
}

